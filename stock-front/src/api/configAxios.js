import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios'

import config from '../config/config';
Vue.use(VueAxios, axios)

export default {
  conf: function() {
    axios.defaults.baseURL = config.api.url;
  }
}
