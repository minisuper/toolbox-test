import configAxios from './configAxios';
import axios from 'axios';

let getProducts = function() {
  return axios.get('products').then((response) => {
    return response;
  });
};

export default {
  getProducts: getProducts
}
