import configAxios from './configAxios';
import axios from 'axios';

let getStock = function(params) {
  var axiosParams = {
    params: params
  };
  return axios.get('inventories', axiosParams).then((response) => {
    return response;
  });
};

let addStock = function(params) {
  return axios.post('inventories', params).then((response) => {
    return response;
  });
};

let updateStock = function(params) {
  return axios.patch('inventories', params).then((response) => {
    return response;
  });
};

export default {
  getStock: getStock,
  addStock: addStock,
  updateStock: updateStock
}
