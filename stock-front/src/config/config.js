var env = require('./env.js');

var config = {
  development: require('./dev.env.js'),
  // production: require('./prod.env.js'),
}

module.exports = config[env]
