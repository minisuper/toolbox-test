require 'spec_helper'

describe InventoriesController do
  describe 'GET #index' do
    before do
      @product = Product.first
      @inventories = Inventory.where(:product_id => @product.id).order(created_at: :asc)
    end

    it "return product id param in the first inventory object returned" do
      get :index, params: {product_id: @product.id, order_by: "date_asc"} , :format => :json
      response_body = JSON.parse(response.body)
      expect(response_body["data"][0]["attributes"]["product"]["id"]).to eq(@product.id)
    end

    it "return the first inventory id with this order and with product filtered" do
      get :index, params: {product_id: @product.id, order_by: "date_asc"} , :format => :json
      response_body = JSON.parse(response.body)
      expect(response_body["data"][0]["id"]).to eq(@inventories.first.id.to_s)
    end
  end

  describe 'POST #create' do
    before do
      @product = Product.all.order(id: :desc).first
      @not_product_id = @product.id + 1
      @quantity = 100
      @price = 1.5
      @note = "Created from rspec"
    end

    it "return true in error. Product doesn't exist" do
      post :create, params: {product_id: @not_product_id, quantity: @quantity, price: @price, note: @note} , :format => :json
      response_body = JSON.parse(response.body)
      expect(response_body["error"]).to eq(true)
    end

    it "return the quantity of the new intentory for the last product" do
      post :create, params: {product_id: @product.id, quantity: @quantity, price: @price, note: @note} , :format => :json
      response_body = JSON.parse(response.body)
      expect(response_body["inventory"]["quantity"]).to eq(@quantity)
    end
  end

  describe 'PATCH #update' do
    before do
      @inventory = Inventory.first
      @quantity = @inventory.quantity + 100
      @not_quantity = @inventory.quantity - 1
    end

    it "return the new quantity for the inventory" do
      patch :update, params: {id: @inventory.id, quantity: @quantity} , :format => :json
      response_body = JSON.parse(response.body)
      expect(response_body["inventory"]["quantity"]).to eq(@quantity)
    end

    it "return true in error object. Incorrect quantity" do
      patch :update, params: {id: @inventory.id, quantity: @not_quantity} , :format => :json
      response_body = JSON.parse(response.body)
      expect(response_body["error"]).to eq(true)
    end
  end
end
