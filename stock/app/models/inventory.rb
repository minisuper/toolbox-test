class Inventory < ApplicationRecord
  belongs_to :product

  scope :between, -> (date_from, date_to) { where("inventories.created_at >= ?", "#{date_from}").where("inventories.created_at <= ?", "#{date_to}") }

end
