class InventorySerializer < ActiveModel::Serializer
  attributes :created_at, :quantity, :price, :note, :product
end
