class ProductsController < ApplicationController

  api :GET, '/products', "Products list"
  def index
    products = Product.all
    msg = {:error => false, :products => products}
    render status: 200, json: msg
  end

end
