class InventoriesController < ApplicationController

  api :GET, '/inventories', "Show product inventory"
  param :product_id, :number, :desc => "Product ID filter", :required => false
  param :order_by, ["date_asc", "date_desc", "product_name_asc", "product_name_desc"], :desc => "Order by creation date or product", :required => false
  param :date_from, DateTime, :desc => "Get inventory from", :required => false
  param :date_to, DateTime, :desc => "Get inventory to", :required => false
  param :page, :number, :desc => "Page number. Default 1", :required => false
  param :per_page, :number, :desc => "Quantity of items per page. Default: 10", :required => false
  def index
    stock = Inventory.where(nil)
    stock = stock.where(:product_id => params[:product_id].to_i.abs) if params[:product_id].present?
    stock = stock.between(params[:date_from], params[:date_to]) if params[:date_from].present? && params[:date_to].present?

    if params[:order_by].present?
      case params[:order_by]
      when "date_asc"
        stock = stock.order(created_at: :asc)
      when "date_desc"
        stock = stock.order(created_at: :desc)
      when "product_name_asc"
        stock = stock.joins(:product).includes(:product).order("products.name ASC")
      when "product_name_desc"
        stock = stock.joins(:product).includes(:product).order("products.name DESC")
      else
        stock = stock.order(:id)
      end
    end

    page = 1
    per_page = 10
    page = params[:page] if params[:page].present?
    per_page = params[:per_page] if params[:per_page].present?
    paginate stock, page: page, per_page: per_page
  end

  api :POST, '/inventories', "Add product to inventory"
  param :product_id, :number, :desc => "Product ID", :required => true
  param :quantity, :number, :desc => "Quantity to add", :required => true
  param :price, String, :desc => "Total price", :required => true
  param :note, String, :desc => "Note", :required => true
  def create
    product = Product.find_by_id(params[:product_id])
    json_r = {:error => true, :message => "The product #{params[:product_id]} doesn't exist"}
    status = 400
    unless product.nil?
      inventory = Inventory.create(product_id: product.id, quantity: params[:quantity], price: params[:price].to_f, note: params[:note])
      if inventory
        json_r = {:error => false, :inventory => inventory}
        status = 201
      else
        json_r = {:error => true, :messasge => "Error creating inventory"}
        status = 400
      end
    end
    render status: status, json: json_r
  end

  api :PATCH, '/inventories/:id', "Decrement quantity for a product of inventory"
  param :id, :number, :desc => "Product ID", :required => true
  param :quantity, :number, :desc => "New quantity. The quantity must be less than current", :required => true
  def update
    inventory = Inventory.find_by_id(params[:id])
    json_r = {:error => true, :message => "The inventory #{params[:product_id]} doesn't exist"}
    status = 400
    unless inventory.nil?
      # TODO: soft delete using paranoid gem or something like
      if params[:quantity].to_i > inventory.quantity
        inventory.quantity = params[:quantity].to_i
        inventory.save
        if inventory.valid?
          json_r = {:error => false, :inventory => inventory}
          status = 200
        else
          json_r = {:error => true, :messasge => "Error updating inventory"}
          status = 400
        end
      else
        json_r = {:error => true, :messasge => "Incorrect quantity param"}
        status = 400
      end
    end
    render status: status, json: json_r
  end

end
