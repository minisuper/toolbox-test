Rails.application.routes.draw do
  apipie
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :inventories do
    collection do
      get '', to: 'inventories#index'
      post '', to: 'inventories#create'
      patch '', to: 'inventories#update'
    end
  end

  resources :products do
    collection do
      get '', to: 'products#index'
    end
  end
end
