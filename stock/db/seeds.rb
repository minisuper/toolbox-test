# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

products = []
5.times do |i|
  products.push(Product.create(name: "Product ##{i}", descrpition: "A product."))
end

now = DateTime.now

15.times do |i|
  now = now - i*3
  products.each do |product|
    qty = rand(10..800)
    unit_price = rand(1..5)
    Inventory.create(product_id: product.id, quantity: qty, price: unit_price*qty, note: "Unit price: " + unit_price.to_s, created_at: now, updated_at: now)
  end
end
