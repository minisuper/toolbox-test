class CreateInventories < ActiveRecord::Migration[5.0]
  def change
    create_table :inventories do |t|
      t.integer :quantity
      t.text :note
      t.decimal :price
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
