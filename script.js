function FizzBuzz(ints) {
  let strings = [];
  ints.forEach((integer) => {
    let by5 = integer % 5;
    let by3 = integer % 3;
    if (by5 == 0 && by3 == 0) {
      strings.push('FizzBuzz');
      return;
    }
    if (by3 == 0) {
      strings.push('Fizz');
      return;
    }
    if (by5 == 0) {
      strings.push('Buzz');
      return;
    }
    let number = integer;
    let integerSplited = 0;
    while(number > 0) {
      integerSplited = number % 10;
      number = parseInt(number / 10);
      if (integerSplited == 3) {
        strings.push('Fizz');
        break;
      }
      if (integerSplited == 5) {
        strings.push('Buzz');
        break;
      }
    }
  });
  return strings;
}

let ints = [15, 2332, 7676, 18];
console.log("Array of integers: ", ints);
console.log("Output: ", FizzBuzz(ints));
