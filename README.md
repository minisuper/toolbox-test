# Toolbox test

## Structure
```
stock folder -> Ruby On Rails API source (You will need Ruby 2.4.0 & Rails 5.0.1 or better)
stock-front folder -> VueJS Front End (You will need Node 8.2.1 and npm 5.3.0)
script.js -> Script for challenge 1 (JavaScript version)
script.php -> Script for challenge 1 (PHP version)
```

## Challenge 1

- script.js: JavaScript version
- script.php: PHP version

## Challenge 2

### API (Ruby On Rails)
#### Setup Environment on Ubuntu 16.04 (API)

- Install packages  


```
$ sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev
```

- Install rbenv

```
$ cd ~/   
$ git clone https://github.com/rbenv/rbenv.git ~/.rbenv   
$ echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc   
$ echo 'eval "$(rbenv init -)"' >> ~/.bashrc   
$ source ~/.bashrc   
$ git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build   
$ echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc   
$ source ~/.bashrc
```

- Install Ruby 2.4.0 && Rails 5.0.1

```
$ rbenv install 2.4.0  
$ rbenv global 2.4.0  
$ gem install bundler  
$ gem install rails -v 5.0.1  
$ rbenv rehash
```  
- Now we need install dependences

First, go to *stock* folder in project and
```
$ bundle install
```

- Run Migrations & Seed
```
$ rake db:migrate
$ rake db:seed
$ rake RAILS_ENV=test db:seed
```

- You can run the **Unit tests (using rspec)**
```
$ rspec
```

- Run API in dev mode
```
$ rails s
```
**The API will be run on *http://localhost:3000***

### API Docs
You can check the API endpoints docs in **http://localhost:3000/apipie**

### Front End (VueJS 2)
You need **NodeJS & npm**.  
Go to *stock-front* folder, install dependences and run in dev mode:

```
$ npm install
$ npm run dev
```
**The Front will be run on *http://localhost:8080***

## Challenge 4

### Subir archivos
- Tener una lista blanca de tipos de archivos permitidos. Esta lista la pondría en el server side
- Usaría experesiones regulares para asegurarme que no haya segundas extensiones, y que el nombre del archivo cumple ciertas condiciones que se deben definir según el tipo de archivo que se espera.
- Me aseguraría que el tamanio esté entre un máximo y mínimo, también a definir dependiendo del archivo que se espera.
- Escanearía los archivos con algún antivirus. Según el resultado de éste, tomaria alguna decisión, posiblemente eliminarlo, reportarlo, etc.
- Renombraría el archivo siguiendo alguna nomenclatura pre-determinada.
- Posiblemente organice los archivos en folders por fecha / tipo o algún otro criterio conveniente, dependiendo qué información me gustaría almacenar y consultar más adelante.

### Caché en el lado cliente
Aplicaría caché en el lado cliente para incrementar la performance. Usando HTTP caching se puede tener un mejor control de las peticiones que hace el cliente hacia el servidor, limitándola y utilizando así una copia de la caché.

### Diferencia entre SOAP y REST

**Rest:** Es un tipo de arquitectura de software que usa HTTP como protocolo de comunicación. Cada petición contiene toda la información necesaria para ejecutarla (no hay  necesidad de estado).

**SOAP:** Es un protocolo que utiliza documentos XML, al igual que Rest, no necesita estados. Bajo TCP puede ser usado sobre HTTP u otros protocolos.
