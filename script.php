<?php

function FizzBuzz(array $ints) {
  $strings = [];
  foreach ($ints as $integer) {
    $by5 = $integer % 5;
    $by3 = $integer % 3;
    if ($by5 == 0 && $by3 == 0) {
      $strings[] = 'FizzBuzz';
      continue;
    }
    if ($by3 == 0) {
      $strings[] = 'Fizz';
      continue;
    }
    if ($by5 == 0) {
      $strings[] = 'Buzz';
      continue;
    }
    $number = $integer;
    while($number > 0) {
      $integerSplited = $number % 10;
      $number = intval($number / 10);
      if ($integerSplited == 3) {
        $strings[] = 'Fizz';
        break;
      }
      if ($integerSplited == 5) {
        $strings[] = 'Buzz';
        break;
      }
    }
  }
  return $strings;
}

$ints = [15, 2332, 7676, 18];

echo "Array of integers: \n";
echo print_r($ints, true) . "\n";
echo "Output: \n";
echo print_r(FizzBuzz($ints), true);

?>
